//This page will be the parent for all the pageObject pages to pass the instance of driver. 
package com.booker.coreFramework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class AbstractWebPage {
	
	protected WebDriver driver;
	
	public AbstractWebPage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(this.driver, this);
	}

}
