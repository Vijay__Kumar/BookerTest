package com.booker.coreFramework;

import java.security.InvalidParameterException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;

import com.booker.configuration.Context;

@ContextConfiguration(classes = Context.class)
public class WebDriverFactory {
	
	@Value("${runMode}")
	private String runMode;

	
	public WebDriver initializeWebDriver() {
		
		WebDriver driver = null;

		switch (runMode) 
		{
		
			case "Chrome":
				System.setProperty("webdriver.chrome.driver", "chromedriver");
				driver = new ChromeDriver();
				break;
		
			case "Firefox":
				System.setProperty("webdriver.gecko.driver", "geckodriver");
				driver = new FirefoxDriver();
				break;
		
			default	: throw new InvalidParameterException(runMode + "is not a valid parameter - RunMode environment variable is not set!");
		}
		
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		return driver;
	}
}