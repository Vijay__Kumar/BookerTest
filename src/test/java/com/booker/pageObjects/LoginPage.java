package com.booker.pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.booker.coreFramework.AbstractWebPage;

public class LoginPage extends AbstractWebPage{
	
	@FindBy(how = How.CSS, using = ".nav.navbar-nav li:nth-child(2) a")
	private WebElement navigateToLogin;

	@FindBy(how = How.CSS, using = "#username")
	private WebElement userNameField;
	
	@FindBy(how = How.CSS, using = "#password")
	private WebElement userPasswordField;
	
	@FindBy(how = How.CSS, using = "#doLogin")
	private WebElement loginBtn;
	
	
	public LoginPage(WebDriver driver) {
		super(driver);
	}
	
	
	public void navigateToLogin(){
		navigateToLogin.click();
	}
	
	public void enterUserName(String Username){
		userNameField.sendKeys(Username);
	}
	
	public void enterPassword(String Password){
		userPasswordField.sendKeys(Password);
	}
	
	public LandingPage clickLogin(){
		loginBtn.click();
		return new LandingPage(driver);
	}
}
