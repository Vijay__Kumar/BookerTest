package com.booker.stepDefinitions;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.booker.coreFramework.AbstractTestCase;
import com.booker.pageObjects.LandingPage;
import com.booker.pageObjects.LoginPage;

import cucumber.api.DataTable;
import cucumber.api.java8.En;


public class EntriesStepDefs extends AbstractTestCase implements En{
	
	private LoginPage loginPage;
	private LandingPage landingPage;
	
	public EntriesStepDefs(){
		
		Given("^the admin-user is able to login to action the entries\\.$", (DataTable userData) -> {
		loginPage = goToLoginPage();
		loginPage.navigateToLogin();
		loginPage.enterUserName(userData.asList(String.class).get(0));
		loginPage.enterPassword(userData.asList(String.class).get(1));
		});

	
		When("^user navigates to home page using admin login$", () -> landingPage = loginPage.clickLogin());

	
		Then("^user creates an Entry$", (DataTable Entry) -> {
			landingPage
				.provideEntries(Entry.asList(String.class).get(5),
			            		Entry.asList(String.class).get(6),
			            		Entry.asList(String.class).get(7),
			            		Entry.asList(String.class).get(8),
			            		Entry.asList(String.class).get(9)); 
			landingPage
				.createAnEntry();
			Assert.assertEquals(driver.findElement(By.xpath("//p[contains(.,'hotelTestMain')]")).getText(), Entry.asList(String.class).get(5));
		});
	

		Then("^user deletes an Entry$", () -> {
			landingPage
				.deleteEntry();
			Assert.assertNotEquals(driver.findElement(By.xpath("//p[contains(.,'hotelTestMain')]")), "hotelTestMain");
		});

	
		Then("^user logs off$", () -> landingPage.logOffAdmin());
		

		Then("^user creates multiple Entries$", (DataTable Entries) -> {
			List<List<String>> createEntry = Entries.raw();
			landingPage
				.provideEntries(createEntry.get(1).get(0).toString(),
				 		 		createEntry.get(1).get(1).toString(),
				 		 		createEntry.get(1).get(2).toString(),
				 		 		createEntry.get(1).get(3).toString(),
				 		 		createEntry.get(1).get(4).toString());
			landingPage
				.createAnEntry();
			Assert.assertEquals(driver.findElement(By.xpath("//p[contains(.,'hotelTestOne')]")).getText(), createEntry.get(1).get(0).toString());
		
			landingPage
				.provideEntries(createEntry.get(2).get(0).toString(),
				 		 		createEntry.get(2).get(1).toString(),
				 		 		createEntry.get(2).get(2).toString(),
				 		 		createEntry.get(2).get(3).toString(),
				 		 		createEntry.get(2).get(4).toString());
			landingPage
				.createAnEntry();
			Assert.assertEquals(driver.findElement(By.xpath("//p[contains(.,'hotelTestTwo')]")).getText(), createEntry.get(2).get(0).toString());
		});

		Then("^the user quits browser$", () -> landingPage.closeBrowser());

	}
}
