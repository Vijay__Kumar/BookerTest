Feature: Verify the set of automated tests for Create, Delete and Creation of Multiple Entries

  Background: Verify User is logged in.
    Given the admin-user is able to login to action the entries.
      | admin | password |

  @testChallenge
  Scenario: Create an Entry
    When user navigates to home page using admin login
    Then user creates an Entry
      | HotelName     | Address              | Owner          | PhoneNo    | Email               |
      | hotelTestMain | 01 TestRoad VJ12 3AB | vktechnologies | 0948590285 | Testuser1@gmail.com |
    And the user quits browser

  @testChallenge
  Scenario: Delete an Entry
    When user navigates to home page using admin login
    Then user deletes an Entry
    Then the user quits browser

  @testChallenge
  Scenario: Creation of Multiple Entries
    When user navigates to home page using admin login
    Then user creates multiple Entries
      | HotelName    | Address              | Owner          | PhoneNo    | Email                           |
      | hotelTestOne | 01 TestRoad VJ12 3AB | vktechnologies | 0948590285 | vijaykumar@vktechnologies.co.uk |
      | hotelTestTwo | 02 TestRoad VJ12 3AB | vktechnologies | 0452852444 | vijaykumar@vktechnologies.co.uk |
    And the user quits browser
